import React from 'react'
import { connect } from 'react-redux'

import { loadPlanets } from '../actions'
import Planets from '../components/Planets'

class Index extends React.Component {
  static async getInitialProps(props) {
    const { store, isServer } = props.ctx

    if (!store.getState().placeholderData) {
      store.dispatch(loadPlanets())
    }

    return { isServer }
  }

  render() {
    return <Planets />
  }
}

export default connect()(Index)
