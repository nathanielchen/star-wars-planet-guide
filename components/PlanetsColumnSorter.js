import React from 'react'
import { compose } from 'redux'
import orderBy from 'lodash/orderBy'
import * as resolve from 'table-resolver'
import * as Table from 'reactabular-table'
import * as sort from 'sortabular'
import PropTypes from 'prop-types'
import * as search from 'searchtabular'
import { NF, PRETTIFY_NUMBER } from '../shared/constant'

class MSColumnSorter extends React.Component {
  constructor(props) {
    super(props)

    this.state = {
      rows: this.props.planetsData, // initial rows
      sortingColumns: this.props.sortingColumns, // Defaultre sorting column
      columns: this.getColumns(), // initial columns

      searchColumn: 'name',
      query: {}, // Search query
    }
    this.onSelectedPlanet = this.onSelectedPlanet.bind(this)
  }

  onSelectedPlanet = planet => this.props.showModalHandler(planet)

  columnsMap = sortableHeader => {
    return [
      {
        property: 'surface_water',
        cell: {
          formatters: [
            (surface_water, extra) => {
              return (
                <img
                  onError={this.handleError}
                  src={`/static/${extra.rowData.name.split(' ').join('-')}.jpg`}
                  className="planet_image"
                />
              )
            },
          ],
        },
        header: {
          label: 'Planet',
        },
      },
      {
        property: 'name',
        cell: {
          formatters: [search.highlightCell],
        },
        header: {
          label: 'Name',
          formatters: [sortableHeader],
        },
      },
      {
        property: 'population',
        cell: {
          formatters: [
            population => {
              return population !== 'unknown' ? (
                <span>{PRETTIFY_NUMBER(population)}</span>
              ) : (
                '-'
              )
            },
          ],
        },
        header: {
          label: 'Population',
          formatters: [sortableHeader],
        },
      },
      {
        property: 'diameter',
        cell: {
          formatters: [diameter => <span>{NF.format(diameter)} km</span>],
        },
        header: {
          label: 'Diameter',
        },
      },
      {
        property: 'terrain',
        header: {
          label: 'Terrain',
        },
      },

      {
        property: 'residents',
        cell: {
          formatters: [
            (residents, extra) => (
              <span
                onClick={() => {
                  this.onSelectedPlanet(extra)
                }}
              >
                <img src="/static/chevron_right.svg" />{' '}
              </span>
            ),
          ],
        },
        header: {
          label: '',
        },
      },
    ]
  }

  getColumns() {
    const sortable = sort.sort({
      getSortingColumns: () => this.state.sortingColumns || {},
      onSort: selectedColumn => {
        this.setState({
          sortingColumns: sort.byColumns({
            sortingColumns: this.state.sortingColumns,
            selectedColumn,
          }),
        })
      },
    })
    const sortableHeader = this.props.handleSortingColumns(
      sortable,
      () => this.state.sortingColumns
    )

    return this.columnsMap(sortableHeader)
  }

  render() {
    const { columns, rows, sortingColumns, searchColumn, query } = this.state

    const composed = compose(
      sort.sorter(
        { columns: columns, sortingColumns, sort: orderBy },
        resolve.resolve({
          columns: columns,
          method: resolve.nested,
        })
      )
    )(search.multipleColumns({ columns, query })(rows))

    return (
      <div>
        <div className="search-container">
          <span>Search</span>
          <search.Field
            column={searchColumn}
            query={query}
            columns={columns}
            rows={rows}
            onColumnChange={searchColumn => this.setState({ searchColumn })}
            onChange={query => this.setState({ query })}
          />
        </div>
        <Table.Provider
          className="table table-striped table-bordered"
          columns={columns}
        >
          <Table.Header />
          <Table.Body rows={composed} rowKey="name" />
        </Table.Provider>
      </div>
    )
  }
}

MSColumnSorter.propTypes = {}

export default MSColumnSorter
