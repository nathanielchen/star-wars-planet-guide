import React, { Component } from 'react'
import { connect } from 'react-redux'
import Wrapper from './ui/Wrapper'
import Container from './ui/Container'
import Header from './ui/Header'
import Modal from './ui/Modal'
import PlanetDetails from './PlanetDetails'

import { closePlanetModal, slectedPlanet } from '../actions'

import PlanetsColumnSorter from './PlanetsColumnSorter'
import {
  default_sortingColumns,
  handleSortingColumns,
} from '../shared/sort_utility'

class Planets extends Component {
  constructor(props) {
    super(props)
  }

  closeModalHandler = () => {
    this.props.dispatch(closePlanetModal())
  }

  showModalHandler = planets => {
    this.props.dispatch(slectedPlanet(planets))
  }

  render() {
    const { error, planetsData, selectedPlanetResidents } = this.props
    return (
      <Wrapper>
        <Modal
          show={this.props.showPlanetModal}
          modalClosed={this.closeModalHandler}
        >
          <PlanetDetails />
        </Modal>

        <Header />
        <Container mainBody>
          <PlanetsColumnSorter
            showModalHandler={this.showModalHandler}
            planetsData={planetsData.results}
            sortingColumns={default_sortingColumns}
            handleSortingColumns={handleSortingColumns}
          />
        </Container>
        <style jsx global>{`
          table .sort {
            cursor: pointer;
            cursor: hand;
            float: right;
          }

          table .sort-order {
            float: right;
            color: #ff6600;
          }

          table .sort:after {
            font-family: FontAwesome;
            font-size: 1em;
          }

          table .sort-none:after {
            content: '\f0dc';
          }

          table .sort-asc:after {
            content: '\f0de';
          }

          table .sort-desc:after {
            content: '\f0dd';
          }

          thead {
            font-size: 11px;
            color: black;
            border-top: 1px solid #ece9f0;
            background-color: #a09117;

            th {
              flex: 1;
              padding: 16px 28px;
            }
          }

          tbody {
            font-size: 12px;
            color: white;
            border-top: 1px solid #ece9f0;
            background-color: black;
            text-align: center;
            td {
              flex: 1;
              padding: 0 25px;
            }
          }

          .planet_image {
            box-shadow: 0 0 3em rgba(0, 0, 0, 0.5);
            width: 80px;
            height: auto;
            vertical-align: middle;
          }

          .image_row {
            position: relative;
            -ms-flex-direction: column;
            flex-direction: column;
            display: -ms-flexbox;
            display: flex;
            height: 100%;
          }
          .image_info {
            font-size: 0.7rem;
            font-weight: 400;
            margin: 0 0 0.7rem;
            text-align: center;
          }
        `}</style>
      </Wrapper>
    )
  }
}
export default connect(state => state)(Planets)
