import React, { Component } from 'react'

class Wrapper extends Component {
  render() {
    return (
      <div className="wrapper center">
        {this.props.children}
        <style jsx>{`
          .wrapper {
            width: 100%;
            height: 100%;
          }
        `}</style>
      </div>
    )
  }
}

export default Wrapper
