import React, { Component } from 'react'
import Aux from './Aux'
import Backdrop from './Backdrop'

class Modal extends Component {
  shouldComponentUpdate(nextProps, nextState) {
    return (
      nextProps.show !== this.props.show ||
      nextProps.children !== this.props.children
    )
  }

  render() {
    return (
      <Aux>
        <Backdrop show={this.props.show} clicked={this.props.modalClosed} />

        <div
          className="modal"
          style={{
            transform: this.props.show ? 'translateY(0)' : 'translateY(-100vh)',
            opacity: this.props.show ? '1' : '0',
          }}
        >
          <button onClick={this.props.modalClosed} className="close">
            <svg className="close_svg" viewBox="0 0 10 10">
              <path d="M0,0 L10,10 M10,0 L0,10" />
            </svg>
          </button>
          {this.props.children}
        </div>

        <style jsx>{`
          .modal {
            position: fixed;
            z-index: 500;
            border-radius: 10px;
            background-color: #172352;
            width: 514px;
            height: 432px;
            box-shadow: 1px 1px 1px black;
            padding: 16px;
            left: 15%;
            top: 20%;
            box-sizing: border-box;
            transition: all 0.3s ease-out;
          }

          @media (min-width: 900px) {
            .modal {
              left: calc(50% - 208px);
            }
          }
          .close {
            stroke: #333;
            stroke-width: 1px;
            width: 35px;
            height: 35px;
            position: absolute;
            padding: 0;
            border: none;
            right: 0;
            top: 0px;
            border: none;
            background: transparent;
            cursor: pointer;
          }
          .close_svg {
            border: none;
            width: 10px;
            height: 10px;
            outline: none;
          }
        `}</style>
      </Aux>
    )
  }
}

export default Modal
