import Container from './Container'
import Router from 'next/router'

function Header({ title }) {
  return (
    <div className="header">
      <Container>
        <img
          className="logo"
          src={`/static/logo.svg`}
          alt="logo"
          onClick={() => {
            Router.push('/')
          }}
        />
      </Container>
      <style jsx>{`
        .header {
          margin: auto;
          padding: 0.4375rem 1.375rem;
          background-color: #000;
        }
        .logo {
          width: 50px;
        }
      `}</style>
    </div>
  )
}

export default Header
