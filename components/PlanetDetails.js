import { connect } from 'react-redux'
import ErrorMessage from './ui/ErrorMessage'
import Wrapper from './ui/Wrapper'
import Container from './ui/Container'
import { NF, PRETTIFY_NUMBER } from '../shared/constant'

function PlanetDetails({ error, selectedPlanet, selectedPlanetResidents }) {
  return (
    <Wrapper>
      {selectedPlanet ? (
        <Wrapper>
          <h2>{selectedPlanet.rowData.name}</h2>
          <img
            src={`/static/${selectedPlanet.rowData.name
              .split(' ')
              .join('-')}.jpg`}
            className="planet_image_big"
          />
          <div>Rotation Period: {selectedPlanet.rowData.rotation_period}</div>
          <div>Orbital Period: {selectedPlanet.rowData.orbital_period}</div>
          <div>Diameter: {NF.format(selectedPlanet.rowData.diameter)} km</div>
          <div>Climate: {selectedPlanet.rowData.climate}</div>
          <div>Gravity: {selectedPlanet.rowData.gravity}</div>
          <div>Terrain: {selectedPlanet.rowData.terrain}</div>
          <div>Surface Water: {selectedPlanet.rowData.surface_water}%</div>
          <div>
            Population: {PRETTIFY_NUMBER(selectedPlanet.rowData.population)}
          </div>
          {selectedPlanetResidents ? (
            <div>Residents: {selectedPlanetResidents.name}</div>
          ) : null}
        </Wrapper>
      ) : (
        <h4>Loading Planet Details...</h4>
      )}
      <ErrorMessage error={error} />

      <style jsx>{`
        .planet_image_big {
          width: 100px;
        }
      `}</style>
    </Wrapper>
  )
}

export default connect(state => state)(PlanetDetails)
