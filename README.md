      _   _       _   _                 _      _  
     | \ | | __ _| |_| |__   __ _ _ __ (_) ___| |
     |  \| |/ _` | __| '_ \ / _` | '_ \| |/ _ | |
     | |\  | (_| | |_| | | | (_| | | | | |  __| |
     |_| \_|\__,_|\__|_| |_|\__,_|_| |_|_|\___|_|

# Star wars planet guide

## Get started

Install it and run:

- Install nvm (if you don't have it)

```bash
- run `npm intall`
- run `npm run dev`

# or

- run `yarn`
- run `yarn build`
- run `yarn dev`
```


Install next, react, redux, redux saga, axiosstyled-jsx-plugin-sass
```bash
yarn add next next-redux-saga next-redux-wrapper
yarn add react redux redux-saga
yarn add axios
yarn add styled-jsx-plugin-sass
```

## Star Wars planet guide

This project is based on a public API available at https://swapi.co/

You are free to use any libraries to complete this project. Only React.js is compulsory.
We would prefer you to create this on a public repository.

Project aim
To allow Star Wars fans to browse and learn about the different planets in the Star Wars EU

Requirements& User stories
  1. The main page must contain a table with all the planets available in the galaxy

  2. The tablemustinclude the planets’ name, population and terrain details

  3. As a user, I can sort the planets by name and/or population

  4. As a user, I can filter planets by name

  5. As a user, I can see more details about a planet by clicking on its corresponding row in the table 
    
      a.  This should include all planet-related data

      b. This should also include residents as a list of names

      c. There is no need to include films

## Evaluation
We want to see not only youradherence to best practices & clean, modularcode, but also your UI/UX talent so feel free to add and explain any extra functionality provided that the main requirements are met.
When you bring this in on the day, as part of the interview we will ask you to add additional functionality.

