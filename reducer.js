import { actionTypes } from './actions'

export const initialState = {
  planetsData: null,
  selectedPlanet: null,
  showPlanetModal: false,
  selectedPlanetResidents: null,
  error: false,
}

function reducer(state = initialState, action) {
  switch (action.type) {
    case actionTypes.FAILURE:
      return {
        ...state,
        ...{ error: action.error },
      }

    case actionTypes.LOAD_PLANETS_SUCCESS:
      return {
        ...state,
        ...{ planetsData: action.data },
      }

    case actionTypes.LOAD_SELECTED_PLANET_RESIDENTS_SUCCESS:
      return {
        ...state,
        ...{ selectedPlanetResidents: action.data },
      }

    case actionTypes.LOAD_SELECTED_PLANET:
      return {
        ...state,
        ...{ showPlanetModal: true, selectedPlanet: action.planet },
      }

    case actionTypes.CLOSE_PLANET_MODAL:
      return {
        ...state,
        ...{ showPlanetModal: false },
      }

    default:
      return state
  }
}

export default reducer
