import React from 'react'

import { slectedPlanet } from '../actions'

export const default_sortingColumns = {}

export const handleSortingColumns = (sortable, getSortingColumns) => {
  return (value, { columnIndex }) => {
    const sortingColumns = getSortingColumns() || []

    return (
      <div style={{ display: 'inline' }}>
        <span className="value">{value}</span>
        {React.createElement(
          'span',
          sortable(value, {
            columnIndex,
          })
        )}
        {sortingColumns[columnIndex] ? (
          <span className="sort-order">
            {sortingColumns[columnIndex].position
              ? sortingColumns[columnIndex].position + 1
              : 1}
          </span>
        ) : (
          <span className="sort-order" />
        )}
      </div>
    )
  }
}
