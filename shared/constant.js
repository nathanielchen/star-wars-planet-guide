export const CHECKBOX_THEME = {}

export const NF = new Intl.NumberFormat()

export const PRETTIFY_NUMBER = value => {
  var thousand = 1000
  var million = 1000000
  var billion = 1000000000
  if (value < thousand) {
    return String(value)
  }

  if (value >= thousand && value <= 1000000) {
    return Math.round(value / thousand) + ' thousand'
  }

  if (value >= million && value <= billion) {
    return Math.round(value / million) + ' millions'
  } else if (value >= billion) {
    return Math.round(value / billion) + ' billions'
  }
}
