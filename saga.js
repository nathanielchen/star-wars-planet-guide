/* global fetch */

import { all, call, put, take, takeLatest } from 'redux-saga/effects'
import es6promise from 'es6-promise'
import 'isomorphic-unfetch'
import axios from 'axios'

import {
  actionTypes,
  failure,
  loadPlanetsSuccess,
  loadSelectedPlanetResidentsSuccess,
} from './actions'

es6promise.polyfill()

function* loadPlanetsSaga() {
  try {
    const res = yield fetch('https://swapi.co/api/planets/')
    const data = yield res.json()
    yield put(loadPlanetsSuccess(data))
  } catch (err) {
    yield put(failure(err))
  }
}

function* loadSelectedPlanetResidentsSaga(planets) {
  try {
    if (planets.planet.rowData.residents.length > 0) {
      const res = yield fetch(planets.planet.rowData.residents[0])
      const data = yield res.json()
      yield put(loadSelectedPlanetResidentsSuccess(data))
    }
  } catch (err) {
    yield put(failure(err))
  }
}

function* rootSaga() {
  yield all([takeLatest(actionTypes.LOAD_PLANETS, loadPlanetsSaga)])
  yield all([
    takeLatest(
      actionTypes.LOAD_SELECTED_PLANET,
      loadSelectedPlanetResidentsSaga
    ),
  ])
}

export default rootSaga
