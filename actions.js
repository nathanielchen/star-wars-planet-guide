export const actionTypes = {
  FAILURE: 'FAILURE',
  LOAD_PLANETS: 'LOAD_PLANETS',
  LOAD_PLANETS_SUCCESS: 'LOAD_PLANETS_SUCCESS',

  CLOSE_PLANET_MODAL: 'CLOSE_PLANET_MODAL',
  SELECTED_PLANET: 'SELECTED_PLANET',
  LOAD_SELECTED_PLANET: 'LOAD_SELECTED_PLANET',
  LOAD_SELECTED_PLANET_RESIDENTS_SUCCESS:
    'LOAD_SELECTED_PLANET_RESIDENTS_SUCCESS',
}

export function failure(error) {
  return {
    type: actionTypes.FAILURE,
    error,
  }
}

export function loadPlanets() {
  return { type: actionTypes.LOAD_PLANETS }
}

export function loadPlanetsSuccess(data) {
  return {
    type: actionTypes.LOAD_PLANETS_SUCCESS,
    data,
  }
}

export function slectedPlanet(planet) {
  return { type: actionTypes.LOAD_SELECTED_PLANET, planet: planet }
}

export function loadSelectedPlanetResidentsSuccess(data) {
  return {
    type: actionTypes.LOAD_SELECTED_PLANET_RESIDENTS_SUCCESS,
    data,
  }
}

export function closePlanetModal() {
  return { type: actionTypes.CLOSE_PLANET_MODAL }
}
